# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://github.com/drizzer14/fnts/compare/v1.3.3...v1.4.0) (2022-01-24)


### Features

* **either:** make `left` and `right` serializable ([591537f](https://github.com/drizzer14/fnts/commit/591537f90c43dc32f517bf6c429b545992e1d212))
* **maybe:** make `just` and `nothing` serializable ([3fe826f](https://github.com/drizzer14/fnts/commit/3fe826f9eb573afcabdd03aa744ff8334756f5ee))

### [1.3.3](https://github.com/drizzer14/fnts/compare/v1.3.2...v1.3.3) (2022-01-24)


### Bug Fixes

* **maybe:** remove buggy overloads of `foldMap` ([184cd50](https://github.com/drizzer14/fnts/commit/184cd505546eef335d0f0c0b157b1f00466f8c58))

### [1.3.2](https://github.com/drizzer14/fnts/compare/v1.3.1...v1.3.2) (2021-12-28)


### Bug Fixes

* **curry:** remove broken `Args['length'] extends 1` condition ([5a48a55](https://github.com/drizzer14/fnts/commit/5a48a5580141784b1cc69e63745679e1d8da902f))
* get rid of ts-ignore from type declarations of compose and pipeline ([658514f](https://github.com/drizzer14/fnts/commit/658514f3d76b3ef7416f7a823d65f23ce562d1dc))

### [1.3.1](https://github.com/drizzer14/fnts/compare/v1.3.0...v1.3.1) (2021-11-28)

## [1.3.0](https://github.com/drizzer14/fnts/compare/v1.2.0...v1.3.0) (2021-11-27)


### Features

* update build, add `conditional`, remove named exports from sources ([b984fb3](https://github.com/drizzer14/fnts/commit/b984fb37eb520b7df8e7e441f29d676f00636d79))
* update source code ([707a512](https://github.com/drizzer14/fnts/commit/707a512b8452f6f2a49c9c24d03140c225074aa9))

## [1.2.0](https://github.com/drizzer14/fnts/compare/v1.0.1...v1.2.0) (2021-09-28)


### Features

* **either:** add `eitherSync` – synchronous version of `either` ([1a118e5](https://github.com/drizzer14/fnts/commit/1a118e54e5d23ad47c2e17fc5dc7f625b78d07fc))
* **inject:** add `inject` function ([acc0456](https://github.com/drizzer14/fnts/commit/acc0456ae42fd3a8fe0cdb47f1c5e61f7b51b9a5))
* **tap:** add `tap` function ([30804f1](https://github.com/drizzer14/fnts/commit/30804f197877bd90e828ca3e108bb979d768fab7))


### Bug Fixes

* **either:** add JSDoc comments ([72d45b6](https://github.com/drizzer14/fnts/commit/72d45b62e83d127b0616a9d192ca98a684873c37))
* remove excess `[@internal](https://github.com/internal)` JSDoc decorators ([a3680a2](https://github.com/drizzer14/fnts/commit/a3680a2a73aef2c22a025a905d3bb4b0516fc305))

## [1.1.0](https://github.com/drizzer14/fnts/compare/v1.0.1...v1.1.0) (2021-09-18)


### Features

* **either:** add `eitherSync` – synchronous version of `either` ([1a118e5](https://github.com/drizzer14/fnts/commit/1a118e54e5d23ad47c2e17fc5dc7f625b78d07fc))

### [1.0.2](https://github.com/drizzer14/fnts/compare/v1.0.1...v1.0.2) (2021-09-12)


### Bug Fixes

* **either:** add JSDoc comments ([72d45b6](https://github.com/drizzer14/fnts/commit/72d45b62e83d127b0616a9d192ca98a684873c37))
* remove excess `[@internal](https://github.com/internal)` JSDoc decorators ([a3680a2](https://github.com/drizzer14/fnts/commit/a3680a2a73aef2c22a025a905d3bb4b0516fc305))

### [1.0.1](https://github.com/drizzer14/fnts/compare/v1.0.0-rc.8...v1.0.1) (2021-09-06)


### Bug Fixes

* **either:** remove `[@internal](https://github.com/internal)` decorator from identifiers ([ac7d045](https://github.com/drizzer14/fnts/commit/ac7d045a241e04e998373ba71659ef3a456bdbdb))
* **maybe:** remove `[@internal](https://github.com/internal)` decorator from identifiers ([8148dc6](https://github.com/drizzer14/fnts/commit/8148dc6377d72a4fa3ea10e5c08bd6c3184f25da))

## [1.0.0](https://github.com/drizzer14/fnts/compare/v1.0.0-rc.8...v1.0.0) (2021-09-06)

🥳
